package model;

public class Book {
    private String BookName;
    private String Author;
    private String Genre;
    private int PageCount;
    private String Language;
    private double Price;
    private boolean Status;
    private int BookCount;

    public Book() {
    }

    public Book(String bookName, String author, String genre, int pageCount, String language, double price, boolean status, int bookCount) {
        BookName = bookName;
        Author = author;
        Genre = genre;
        PageCount = pageCount;
        Language = language;
        Price = price;
        Status = status;
        BookCount = bookCount;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public int getPageCount() {
        return PageCount;
    }

    public void setPageCount(int pageCount) {
        PageCount = pageCount;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public int getBookCount() {
        return BookCount;
    }

    public void setBookCount(int bookCount) {
        BookCount = bookCount;
    }

    @Override
    public String toString() {
        return "BookName: " + BookName +
                ", Author: " + Author +
                ", Genre: " + Genre +
                ", PageCount: " + PageCount +
                ", Language: " + Language +
                ", Price: " + Price +
                ", BookCount: " + BookCount ;
    }
}
