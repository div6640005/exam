package service;

public interface BookService {
    void  addBook();

    void showBook();

    void updateBook();

    void deleteBook();

    void findBook();

}
