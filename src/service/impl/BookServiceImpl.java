package service.impl;

import globalData.BookData;
import model.Book;
import service.BookService;
import util.InputUtil;

public class BookServiceImpl implements BookService {
    @Override
    public void addBook() {
        int bookCount = InputUtil.inputTypeInteger("How many books do you want to include: ");
        for (int i = 0; i < bookCount; i++) {
            Book book = new Book(InputUtil.inputTypeString("Enter The Book name: ")
                    , InputUtil.inputTypeString("Enter the Book`s Author: ")
                    , InputUtil.inputTypeString("Enter the Book`s genre: ")
                    , InputUtil.inputTypeInteger("Enter the Book`s pages: ")
                    , InputUtil.inputTypeString("Enter the Book`s language: ")
                    , InputUtil.inputTypeDouble("Enter the Book`s price: ")
                    , InputUtil.inputTypeBoolean("Enter the Book`s status: ")
                    , InputUtil.inputTypeInteger("Enter the Book`s count: "));
            BookData.books.add(book);
        }
    }

    @Override
    public void showBook() {
        int count = 0;
        for (int i = 0; i < BookData.books.size(); i++) {
            if (BookData.books.get(i).getStatus()) {
                System.out.println(BookData.books);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("We dont have book");
        }
    }

    @Override
    public void updateBook() {
        Book updatesBook = BookData.books.get(InputUtil.inputTypeInteger("Which Book do you want to update: "));
        int changesCount = InputUtil.inputTypeInteger("How many places will you make changes: ");
        System.out.println("""
                [1] Book`s Name
                [2] Book`s Author
                [3] Book`s Genre
                [4] Book`s Pages
                [5] Book`s Language
                [6] Book`s Price
                [7] Book`s Status
                [8] Book`s count
                What do you want to change:
                 """);
        for (int i = 0; i < changesCount; i++) {
            int option = InputUtil.inputTypeInteger("Enter the option: ");
            switch (option) {
                case 1 -> updatesBook.setBookName(InputUtil.inputTypeString("Enter The Book`s name: "));
                case 2 -> updatesBook.setAuthor(InputUtil.inputTypeString("Enter The Book`s author: "));
                case 3 -> updatesBook.setGenre(InputUtil.inputTypeString("Enter The Book`s genre: "));
                case 4 -> updatesBook.setPageCount(InputUtil.inputTypeInteger("Enter The Book`s page count: "));
                case 5 -> updatesBook.setLanguage(InputUtil.inputTypeString("Enter The Book`s language: "));
                case 6 -> updatesBook.setPrice(InputUtil.inputTypeDouble("Enter The Book`s price: "));
                case 7 -> updatesBook.setStatus(InputUtil.inputTypeBoolean("Enter The Book`s Status: "));
                case 8 -> updatesBook.setBookCount(InputUtil.inputTypeInteger("Enter The Book`s count: "));
                default -> System.out.println("Invalid option!");

            }
        }

    }

    @Override
    public void deleteBook() {
        BookData.books.remove(InputUtil.inputTypeInteger("Which item do you want to delete: "));
    }

    @Override
    public void findBook() {

    }
}
