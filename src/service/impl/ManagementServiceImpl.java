package service.impl;

import util.MenuUtil;

public class ManagementServiceImpl {
    public void management() {
        BookServiceImpl bookService = new BookServiceImpl();
        while (true) {
            int option = MenuUtil.entryApp();
            switch (option) {
                case 0 -> {
                    System.out.println("Good bye!\n");
                    System.exit(-1);
                }
                case 1 -> bookService.addBook();
                case 2 -> bookService.showBook();
                case 3 -> bookService.updateBook();
                case 4 -> bookService.deleteBook();
                case 5 -> bookService.findBook();
                default -> System.out.println("Invalid option!");
            }
        }
    }
}
