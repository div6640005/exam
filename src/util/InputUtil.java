package util;

import java.util.Scanner;

public class InputUtil {
    public static String inputTypeString(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.nextLine();
    }

    public static int inputTypeInteger(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.nextInt();
    }

    public static double inputTypeDouble(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.nextDouble();
    }

    public static boolean inputTypeBoolean(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.nextBoolean();
    }
}
