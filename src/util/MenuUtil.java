package util;

public class MenuUtil {
    public static int entryApp(){
        System.out.println("""
                ----------- Employee Management System --------------
                [0]Exit system
                [1]Add Book
                [2]Show Book
                [3]Update Book
                [4]Delete Book
                [5]Find Book
                """);

        return InputUtil.inputTypeInteger("Choose option: \n");
    }
}
